﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    // This is an unusual but handy use of Vector2, x holds a min value
    // and y a max value for a Random.Range() that will be called later
    public Vector2 rotMinMax = new Vector2(15, 90);
    public Vector2 driftMinMax = new Vector2(.25f, 2);
    public float lifetime = 6f; // Seconds the powerup exists
    public float fadetime = 4f; // seconds it will then fade
    public bool ___________;
    public WeaponType type;
    public GameObject cube;
    public TextMesh letter;
    public Vector3 rotPerSecond;
    public float birthTime;

    void Awake()
    {
        // Find the cube reference
        cube = transform.Find("Cube").gameObject;
        // Find the TextMesh
        letter = GetComponent<TextMesh>();

        // Set a random velocity
        Vector3 vel = Random.onUnitSphere;
        vel.z = 0;
        vel.Normalize();
        vel *= Random.Range(driftMinMax.x, driftMinMax.y);
        this.GetComponent<Rigidbody>().velocity = vel;

        transform.rotation = Quaternion.identity;
        // Quaternion.identity is equal to no rotation
        rotPerSecond = new Vector3(Random.Range(rotMinMax.x, rotMinMax.y), Random.Range(rotMinMax.x, rotMinMax.y), Random.Range(rotMinMax.x, rotMinMax.y));
        InvokeRepeating("CheckOffScreen", 2f, 2f);
        birthTime = Time.time;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Manually rotate the cube child every Update()
        // Multyplying it by Time.time causes the rotation to be time-based
        cube.transform.rotation = Quaternion.Euler(rotPerSecond * Time.time);
        // Fade out the PowerUp over time
        // Given the default values, a powerUp will exist for 10 seconds
        // and then fade out over 4 seconds
        float u = (Time.time - (birthTime + lifetime)) / fadetime;
        // For lifetime seconds, u will be <=0. Then it will transition to 1 over fadeTime seconds
        // If u>=1, destroy this PowerUp
        if (u >= 1)
        {
            Destroy(this.gameObject);
            return;
        }
        // Use u to determine the alpha value of the Cube & letter
        if (u > 0)
        {
            Color c = cube.GetComponent<Renderer>().material.color;
            c.a = 1f - u;
            cube.GetComponent<Renderer>().material.color = c;
            // Fade the letter too, just not as much
            c = letter.color;
            c.a = 1f - (u * 0.5f);
            letter.color = c;
        }
    }

    public void SetType(WeaponType wt)
    {
        // Grab the WeaponDefinition from Main
        WeaponDefinition def = Main.GetWeaponDefinition(wt);
        // Set the color of the cube child
        cube.GetComponent<Renderer>().material.color = def.color;
        letter.text = def.letter;
        type = wt;
    }

    public void AbsorbedBy(GameObject target)
    {
        // This function is called by the Hero class when a powerUp is collected
        // We could tween into the target and shrink in size
        // but for now, just destroy this.gameObject
        Destroy(this.gameObject);
    }

    void CheckOffScreen()
    {
        if(Utils.ScreenBoundsCheck(cube.GetComponent<Collider>().bounds, BoundsTest.offScreen)!= Vector3.zero)
        {
            Destroy(this.gameObject);
        }
    }
}
