﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_1 : Enemy
{
    // # seconds for a full sine wave
    public float waveFrequency = 2;
    // sine wave width in meters
    public float waveWidth = 4;
    public float waveRotY = 45;

    private float birthTime;
    private float x0;
    // Start is called before the first frame update
    void Start()
    {
        birthTime = Time.time;
        x0 = pos.x;   
    }

    // Override the Move function on Enemy
    public override void Move()
    {
        // Because pos is a property, you can't directly set pos.x
        // so get the pos as an editable Vector3
        Vector3 tmp = pos;
        // theta adjusts based on time
        float age = Time.time - birthTime;
        float theta = Mathf.PI * 2 * age / waveFrequency;
        float sin = Mathf.Sin(theta);
        //tmp.x = pos.x + waveWidth * sin;
        tmp.x = x0 + waveWidth * sin;
        pos = tmp;
        // rotate a bit about y
        Vector3 rot = new Vector3(0, sin * waveRotY, 0);
        this.transform.rotation = Quaternion.Euler(rot);
        // Enmey::Move() controls the movement of y
        base.Move();
    }
}
