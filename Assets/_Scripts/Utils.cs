﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoundsTest
{
    center,     // Is the center of the GameObject on screen?
    onScreen,   // Are the bounds entirely on screen?
    offScreen   // Are the bounds entirely off screen?
}
public class Utils : MonoBehaviour
{
    //============================ Bounds Functions ==============================\\
    // Creates bounds that encapsulate the two Bounds passed in
    static public  Bounds BoundsUnion(Bounds b0, Bounds b1)
    {
        if (b0.size == Vector3.zero && b1.size != Vector3.zero)
        {
            return (b1);
        }
        else if (b0.size != Vector3.zero && b1.size == Vector3.zero)
        {
            return (b0);
        }
        else if (b0.size == Vector3.zero && b1.size == Vector3.zero)
        {
            return (b0);
        }
        // Stretch b0 to include the b1.min and b1.max 
        //if both b1.min and b1.max are inside the newly expanded b0, then b0 has expanded to surround b1 as well.
        b0.Encapsulate(b1.min);
        b0.Encapsulate(b1.max);
        return (b0);

    }

    static public  Bounds CombineBoundsOfChildren(GameObject gobj) {
        // Create an empty Bounds b
        Bounds b = new Bounds(Vector3.zero, Vector3.zero);

        // If this GameObject has a Renderer's Bounds
        if(gobj.GetComponent<Renderer>() != null)
        {
            // Expand b to contain the Renderer's Bounds
            b = BoundsUnion(b, gobj.GetComponent<Renderer>().bounds);
        }
        // If this GameObject has a Collider Component
        if(gobj.GetComponent<Collider>() != null)
        {
            b = BoundsUnion(b, gobj.GetComponent<Collider>().bounds);

        }

        foreach(Transform t in gobj.transform)
        {
            b = BoundsUnion(b, CombineBoundsOfChildren(t.gameObject));
        }
        return b;
    }

    static private Bounds _camBounds;
    // Make a static read-only public property camBounds
    static public Bounds camBounds
    {
        get
        {
            if (_camBounds.size == Vector3.zero)
                SetCameraBounds();
            return _camBounds;
        }
    }

    static public void SetCameraBounds(Camera cam = null)
    {
        // If no camera was passed in, use the main Camera
        if (cam == null)
            cam = Camera.main;
        // We assume that
        // The camera is Orthographic
        // The caemra is at a roation of R:[0,0,0]

        // Make Vector3 at the topLeft and bottomRight of the screen coords;
        Vector3 topLeft = new Vector3(0, 0, 0);
        Vector3 bottomRight = new Vector3(Screen.width, Screen.height, 0);

        // Convert these to world coordinates
        // TLN = top-left near
        // BRF = bottom-right far
        Vector3 boundTLN = cam.ScreenToWorldPoint(topLeft);
        Vector3 boundBRF = cam.ScreenToWorldPoint(bottomRight);

        // Adjust their zs to be at the near and far Camera clipping planes
        boundTLN.z += cam.nearClipPlane;
        boundBRF.z += cam.farClipPlane;

        // Find the center on the Bounds
        Vector3 center = (boundTLN + boundBRF) / 2f;
        _camBounds = new Bounds(center, Vector3.zero);

        // Expand _camBounds to encapsulate the extends
        _camBounds.Encapsulate(boundTLN);
        _camBounds.Encapsulate(boundBRF);
    }

    static public Vector3 ScreenBoundsCheck(Bounds bnd, BoundsTest test = BoundsTest.center)
    {
        return BoundsInBoundsCheck(camBounds, bnd, test);
    }

    public static Vector3 BoundsInBoundsCheck(Bounds bigB, Bounds lilB, BoundsTest test = BoundsTest.onScreen)
    {
        // The behavior of this function is different based on the BoundsTest // that has been selected.
        // Get the center of lilB 
        Vector3 pos = lilB.center;
        // Initialize the offset at [0,0,0] 
        Vector3 off = Vector3.zero;
        switch (test)
        { // The center test determines what off (offset) would have to be applied // to lilB to move its center back inside bigB 
            case BoundsTest.center:
                if (bigB.Contains(pos))
                {
                    return (Vector3.zero);
                }
                if (pos.x > bigB.max.x)
                {
                    off.x = pos.x - bigB.max.x;
                }
                else if (pos.x < bigB.min.x)
                {
                    off.x = pos.x - bigB.min.x;
                }
                if (pos.y > bigB.max.y)
                {
                    off.y = pos.y - bigB.max.y;
                }
                else if (pos.y < bigB.min.y)
                {
                    off.y = pos.y - bigB.min.y;
                }
                if (pos.z > bigB.max.z)
                {
                    off.z = pos.z - bigB.max.z;
                }
                else if (pos.z < bigB.min.z)
                {
                    off.z = pos.z - bigB.min.z;
                }
                return (off);
            // The onScreen test determines what off would have to be applied to // keep all of lilB inside bigB 
            case BoundsTest.onScreen:
                if (bigB.Contains(lilB.min) && bigB.Contains(lilB.max))
                {
                    return (Vector3.zero);
                }
                if (lilB.max.x > bigB.max.x)
                {
                    off.x = lilB.max.x - bigB.max.x;
                }
                else if (lilB.min.x < bigB.min.x)
                {
                    off.x = lilB.min.x - bigB.min.x;
                }
                if (lilB.max.y > bigB.max.y)
                {
                    off.y = lilB.max.y - bigB.max.y;
                }
                else if (lilB.min.y < bigB.min.y)
                {
                    off.y = lilB.min.y - bigB.min.y;
                }
                if (lilB.max.z > bigB.max.z)
                {
                    off.z = lilB.max.z - bigB.max.z;
                }
                else if (lilB.min.z < bigB.min.z)
                {
                    off.z = lilB.min.z - bigB.min.z;
                }
                return (off);
            // The offScreen test determines what off would need to be applied to // move any tiny part of lilB inside of bigB 
            case BoundsTest.offScreen:
                bool cMin = bigB.Contains(lilB.min); bool cMax = bigB.Contains(lilB.max); if (cMin || cMax)
                {
                    return (Vector3.zero);
                }
                if (lilB.min.x > bigB.max.x)
                {
                    off.x = lilB.min.x - bigB.max.x;
                }
                else if (lilB.max.x < bigB.min.x)
                {
                    off.x = lilB.max.x - bigB.min.x;
                }
                if (lilB.min.y > bigB.max.y) { off.y = lilB.min.y - bigB.max.y; }
                else if (lilB.max.y < bigB.min.y)
                {
                    off.y = lilB.max.y - bigB.min.y;
                }
                if (lilB.min.z > bigB.max.z)
                {
                    off.z = lilB.min.z - bigB.max.z;
                }
                else if (lilB.max.z < bigB.min.z)
                {
                    off.z = lilB.max.z - bigB.min.z;
                }
                return (off);
        }
        return (Vector3.zero);
    }

    //============================ Transform Functions ===========================\\
    static public GameObject FindTaggedParent(GameObject go)
    {
        // If this gameObject has a tag
        if(go.tag!= "Untagged")
        {
            return go;
        }
        // If there is no parent of this Transform
        if(go.transform.parent == null)
        {
            // We've reached the top of the hierarchy with no interestring tag
            return null;
        }
        return FindTaggedParent(go.transform.parent.gameObject);
    }

    static public GameObject FindTaggedParent(Transform t)
    {
        return FindTaggedParent(t.gameObject);
    }

    //=========================== Materials Functions ============================\\

    static public Material[] GetAllMaterials(GameObject go)
    {
        List<Material> mats = new List<Material>();
        if (go.GetComponent<Renderer>().material != null)
            mats.Add(go.GetComponent<Renderer>().material);
        foreach (Transform t in go.transform)
            mats.AddRange(GetAllMaterials(t.gameObject));
        return mats.ToArray();
    }
}
